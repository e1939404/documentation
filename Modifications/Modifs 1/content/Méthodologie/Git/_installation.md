---
title: "Installation"
weight: 3
date: 2022-01-11T14:19:27Z
draft: false
---

### Installation de Git  

* Téléchargez la dernière version de [Git for Windows installer](https://gitforwindows.org/).

* Lancez le programme d'installation, vous devriez voir l'écran de l'assistant Git Setup. 
* Acceptez les options telles quelles, sauf pour les écrans suivants:  

![git setup 1](http://ml.cm9.ca/cours3d1/images/gitSetup1.png)  

![git setup 2](http://ml.cm9.ca/cours3d1/images/gitSetup2.png)  


![git setup 3](http://ml.cm9.ca/cours3d1/images/gitSetup3.png)  
### Configuration de Git  

Ouvrez une invite de commande et exécutez les commandes suivantes pour configurer votre nom d'utilisateur et votre adresse e-mail Git. Ces informations seront associées à tous les commits que vous créez :

```posh  
C:\WINDOWS\system32>git config --global user.name "Votre username défini dans GitLab"  
C:\WINDOWS\system32>git config --global user.email "Votre courriel du collège"
```  
### Utilisation d'une clé SSH  

Lorsque vous utilisez un dépôt privé sur un serveur Git (exemple: GitLab), vous devez vous authentifier. Vous pouvez le faire via une clé SSH. La plupart des clients Git, tels que GitKraken vous permettent de gérer vos clés SSH. Si vous voulez utiliser une clé SSH à partir de l'invite de commande, vous devez faire ces deux étapes:
*  [Générer une clé SSH sur votre ordinateur](https://www.atlassian.com/fr/git/tutorials/git-ssh)   
*  [Ajouter cette clé à votre compte GitLab](https://docs.gitlab.com/ee/user/ssh.html#add-an-ssh-key-to-your-gitlab-account)  

